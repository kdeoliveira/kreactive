<?php
namespace App\Controller;

use App\Entity\Movie;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;

class DeleteMovieController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @ParamConverter("user", options={"mapping": {"user_id": "id"}})
     * @ParamConverter("movie", options={"mapping": {"movie_id": "id"}})
     *
     * @param User $user
     * @param Movie $movie
     *
     * @return JsonResponse
     */
    public function index(User $user, Movie $movie)
    {
        $movieFinding = $this->entityManager->getRepository(Movie::class)->findOneBy([
            'id' => $movie,
            'user' => $user,
        ]);

        if ($movieFinding instanceof Movie === false) {
            $response = [
                'title' => 'Errors',
                'errors' => 'L\'utilisateur et le film ne correspondent pas'
            ];
            return new JsonResponse($response, 400);
        }

        $this->entityManager->remove($movie);
        $this->entityManager->flush();

        return new JsonResponse([
            'title' => 'Success'
        ]);
    }
}
<?php
namespace App\Controller;

use App\Entity\Movie;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;


class MovieController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function index(Request $request, User $user)
    {
        $data = json_decode($request->getContent(), true);

        $client = new \GuzzleHttp\Client();

        $response = $client->request('GET', 'http://www.omdbapi.com', [
            'query' => [
                't' => $data['search'],
                'apikey' => '4bb2f61f',
            ]
        ]);

        $response = json_decode($response->getBody());

        if ($response->Response === 'False') {
            $message = [
                'title' => 'Errors',
                'errors' => $response->Error
            ];
            return new JsonResponse($message, 400);
        }

        $movie = new Movie();
        $movie
            ->setTitle($response->Title)
            ->setPoster($response->Poster)
            ->setIdImdb($response->imdbID)
            ->setUser($user)
        ;

        $this->entityManager->persist($movie);
        $this->entityManager->flush();

        return new JsonResponse([
            'title' => 'Success',
            'title_movie' => $response->Title,
            'poster' => $response->Poster,
        ]);
    }
}
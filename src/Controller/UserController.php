<?php
namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

class UserController extends AbstractController
{

    /**
     * @var FormFactoryInterface
     */
    private $formFactory;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var SerializerInterface
     */
    private $serializer;

    public function __construct(
        FormFactoryInterface $formFactory,
        EntityManagerInterface $entityManager,
        SerializerInterface $serializer
    ) {
        $this->formFactory = $formFactory;
        $this->entityManager = $entityManager;
        $this->serializer = $serializer;
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $user = new User();
        $data = json_decode($request->getContent(), true);

        $form = $this->formFactory->create(UserType::class, $user);
        $form->submit($data);

        if ($form->isValid() === false) {
            $response = [
                'title' => 'Errors',
                'errors' => (string) $form->getErrors(true, false)
            ];
            return new JsonResponse($response, 400);
        }

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return new Response(
            $this->serializer->serialize($user, 'json')
        );
    }
}
<?php
namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

class ListMovieController extends AbstractController
{
    /**
     * @var SerializerInterface
     */
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    public function index(User $user)
    {
        return new Response($this->serializer->serialize($user, 'json', [
            'attributes' => [
                'movies' => [
                    'title',
                    'poster'
                ]
            ]
        ]));
    }
}
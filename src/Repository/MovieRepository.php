<?php
namespace App\Repository;

class MovieRepository extends \Doctrine\ORM\EntityRepository
{
    public function findMovieByAdding()
    {
        return $this
            ->createQueryBuilder('m')
            ->select('m.poster, m.title, COUNT(m) as total')
            ->groupBy('m.title')
            ->addGroupBy('m.poster')
            ->orderBy('total', 'DESC')
            ->getQuery()
            ->getResult();
    }
}
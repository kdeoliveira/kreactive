<?php
namespace App\Repository;

class UserRepository extends \Doctrine\ORM\EntityRepository
{
    public function findUserHasMovie()
    {
        return $this
            ->createQueryBuilder('u')
            ->select('u.id, u.pseudo, u.email, u.birthday')
            ->join('u.movies', 'm')
            ->getQuery()
            ->getResult();
    }
}



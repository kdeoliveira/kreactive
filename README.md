# Kreactive

## Lancer le projet


Installation des vendors
```console
composer install
docker-compose up -d
```

Le container mysql créé un dossier ./var/mysql en root il faut changer les droits

```console
sudo chown -R user:group ./var/mysql
docker-compose restart
```


Installation de la BDD

```console
bin/console doctrine:database:create
bin/console doctrine:schema:update --force
```

NGINX tourne sur le port 8080
<a href="localhost:8080">Kreactive</a>

## Route

- POST Create User
- Path **/user**
- Params type JSON

```JSON
{
    "email": "kreactive@norsys.fr",
    "pseudo": "kevin",
    "birthday": "15/05/1991"
}
 ```


- POST Add movie
- Path **/user/{idUser}/movie**
- Params type JSON
```JSON
{
    "search": "interstellar"
}
 ```


- GET List movies by user
- Path **/user/{idUser}/movie**

- GET List user has movies
- Path **/user-has-movie**

- GET Best movies by users 
- Path **/best**

- DELETE Delete movie by user
- Path **/user/{idUser}/movie/{idMovie}**
